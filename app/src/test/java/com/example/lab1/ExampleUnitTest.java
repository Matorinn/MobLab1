package com.example.lab1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }
    @Test
    public void test_min() {assertEquals(1 , MainActivity.min(4, 1));}
    @Test
    public void test_min_equals() {assertEquals(1 , MainActivity.min(1, 1));}
    @Test
    public void test_min_big() {assertEquals(13594468 , MainActivity.min(13594468, 13994468));}
    @Test
    public void test_min_minus() {assertEquals(-113 , MainActivity.min(-113, -42));}

    @Test
    public void test_max() {assertEquals(4 , MainActivity.max(4, 1));}
    @Test
    public void test_max_equals() {assertEquals(1 , MainActivity.max(1, 1));}
    @Test
    public void test_max_big() {assertEquals(13994468 , MainActivity.max(13594468, 13994468));}
    @Test
    public void test_max_minus() {assertEquals(-42 , MainActivity.max(-113, -42));}
}